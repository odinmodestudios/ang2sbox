import { Ang2sboxPage } from './app.po';

describe('ang2sbox App', function() {
  let page: Ang2sboxPage;

  beforeEach(() => {
    page = new Ang2sboxPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
